Rails.application.routes.draw do

	namespace :api, defaults: {format: 'json'} do

		namespace :v1 do
			resources :feeds do
			member do
				post 'like_count'
			end
		end   
		resources :comments, only:[:index, :create] 

		end
	end	

		resources :feeds do
			member do
				post 'like_count'
			end
			resources :comments, only:[:index, :create]

		end
		
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end