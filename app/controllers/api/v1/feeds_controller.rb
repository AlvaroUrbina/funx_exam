class Api::V1::FeedsController < ApplicationController
	before_action :set_feed, only: [:show]


	def index
		@feeds = Feed.all
		render json: @feeds
	end

	def show
		render json: @feed
	end

	def like_count

		@feed.like += 1
		@feed.save
		redirect_to feed_path(@feed), notice: "Like succsess"
		
	end

	private
	    # Use callbacks to share common setup or constraints between actions.
	    def set_feed
	    	@feed = Feed.find(params[:id])
	    end

	  end