class Feed < ApplicationRecord
	require 'json'
	require 'open-uri'

	has_many :comments

	
	def self.save_data

		feeds = JSON.load(open('https://www.teleton.cl/feed/?json'))
			data = feeds["items"]
			data.each do |dat|
				row = Feed.new(
					:fecha => dat["fecha"],
					:content => dat["content"],
					:all_content => dat["all_content"],
					:image => dat["image"],
					:link => dat["link"],
					:title => dat["title"],
					:categories => dat["categories"],
					:comments  => dat["comments"],
					:like => dat["like"].to_i
					)
				row.save 
			end
	end
end
